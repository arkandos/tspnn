#include "csv.h"
#include "tour.h"
#include "traveling_salesman.h"


int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s tour.csv [greedy.csv]\n", argv[0]);
        return 1;
    }

    CsvData data = csv_load(argv[1]);
    CostMatrix matrix = cost_matrix_new(data.cities, data.num);
    printf("Tour cost: %lu\n", tour_cost(matrix, data.tour, data.num));

    // shuffle(data.cities, data.num, sizeof(City));
    matrix = cost_matrix_new(data.cities, data.num);

    Tour best_tour = tour_new(data.num);
    traveling_salesman(matrix, best_tour, data.num);

    printf("Best tour cost: %lu\n", tour_cost(matrix, best_tour, data.num));

    if (argc >= 3) {
        csv_print(argv[2], data.cities, best_tour, data.num);
    }

    return 0;
}
