#include <stdio.h>

#include "csv.h"
#include "cost_matrix.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s tour.csv [greedy.csv]\n", argv[0]);
        return 1;
    }

    CsvData data = csv_load(argv[1]);
    CostMatrix matrix = cost_matrix_new(data.cities, data.num);
    printf("%lu\n", tour_cost(matrix, data.tour, data.num));
}
