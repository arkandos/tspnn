#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <unistd.h>

#include "base.h"
#include "traveling_salesman.h"
#include "csv.h"

// [0; 1[
internal float rand_float() {
    return rand() / (RAND_MAX + 1.0);
}

// [min; max]
internal int rand_int(int includingLower, int includingUpper) {
    assert(includingLower <= includingUpper);

    // since casting to int always rounds down, we will
    // never hit excludingUpper, since rand_float() has
    // an excluding upper bound as well.
    int excludingUpper = includingUpper + 1;
    int d = excludingUpper - includingLower;
    return includingLower + (int)(rand_float() * d);
}


internal City city_random(char *name) {
    City result;
    result.name = name;

    // make sure we do not generate cities on the origin - this is a special
    // point we use to indicate <no data> to the NN.
    do {
        result.x = rand_int(-WORLD_RADIUS, WORLD_RADIUS);
        result.y = rand_int(-WORLD_RADIUS, WORLD_RADIUS);
    } while (!result.x && !result.y);

    return result;
}

// Robert Jenkins' 96 bit Mix Function
internal unsigned long mix(unsigned long a, unsigned long b, unsigned long c) {
    a=a-b;  a=a-c;  a=a^(c >> 13);
    b=b-c;  b=b-a;  b=b^(a << 8);
    c=c-a;  c=c-b;  c=c^(b >> 13);
    a=a-b;  a=a-c;  a=a^(c >> 12);
    b=b-c;  b=b-a;  b=b^(a << 16);
    c=c-a;  c=c-b;  c=c^(b >> 5);
    a=a-b;  a=a-c;  a=a^(c >> 3);
    b=b-c;  b=b-a;  b=b^(a << 10);
    c=c-a;  c=c-b;  c=c^(b >> 15);
    return c;
}


int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s out-file.csv\n", argv[0]);
        return 1;
    }

    unsigned long seed = mix(clock(), time(NULL), getpid());
    srand(seed);

    size_t cities_count = rand_int(MIN_CITIES_COUNT, MAX_CITIES_COUNT);
    City *cities = calloc(cities_count, sizeof(City));
    for(size_t i = 0; i < cities_count; ++i) {
        cities[i] = city_random(city_names[i]);
    }

    CostMatrix cost_matrix = cost_matrix_new(cities, cities_count);

    Tour best_tour = tour_new(cities_count);
    traveling_salesman(cost_matrix, best_tour, cities_count);

    csv_print(argv[1], cities, best_tour, cities_count);

    return 0;
}
