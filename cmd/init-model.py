#!/usr/bin/env python

import os
import shutil

import tensorflow as tf
import keras
from keras import layers

MODEL_PATH="./train_data/model"
MAX_CITIES_COUNT=20

model = keras.Sequential()

# Inputs are x/y coordinates of all cities
model.add(layers.Dense(128, activation='relu', input_shape=(MAX_CITIES_COUNT*2,)))
model.add(layers.Dense(256, activation='relu'))
model.add(layers.Dense(2048, activation='relu'))
model.add(layers.Dense(2048, activation='relu'))

# Output is the predicted x/y of the closest city.
model.add(layers.Dense(2))

model.compile(optimizer='adam', loss='mean_squared_error')

if os.path.exists(MODEL_PATH):
    shutil.rmtree(MODEL_PATH)

model.save(MODEL_PATH)
