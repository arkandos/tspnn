#!/bin/sh

mkdir -p train_data/csv/
mkdir -p train_data/svg/

files=()
for i in {1000..9999};
do
    files+=("rand-${i}")
done

cores=$(($(nproc --all) - 1))
if (( $cores < 1 )); then
    cores=1
fi

printf "%s\0" ${files[@]} | xargs -0 -P $cores -I {} ./dist/generate-train-data train_data/csv/{}.csv
printf "%s\0" ${files[@]} | xargs -0 -P $cores -I {} ./dist/tour-to-svg train_data/csv/{}.csv train_data/svg/{}.svg
