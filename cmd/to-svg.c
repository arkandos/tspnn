#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "csv.h"


internal void svg_print_city(FILE *f, City *from, City *to, const char* fill) {
    size_t width = WORLD_RADIUS * 2;
    float radius = width / 100.0f;
    float line_width = radius / 3;
    float padding = 2 * radius;

    float x = to->x + WORLD_RADIUS + padding;
    float y = to->y + WORLD_RADIUS + padding;

    fprintf(f, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" fill=\"%s\">\n", x, y, radius, fill);
    fprintf(f, "  <title>%s</title>\n", to->name);
    fprintf(f, "</circle>");

    float px = from->x + WORLD_RADIUS + padding;
    float py = from->y + WORLD_RADIUS + padding;

    fprintf(f, "<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" stroke=\"red\" stroke-width=\"%f\" />\n", px, py, x, y, line_width);
}


int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s tour.csv output.svg\n", argv[0]);
        return 1;
    }

    CsvData data = csv_load(argv[1]);

    FILE *f = fopen(argv[2], "w+");
    if (!f) {
        fprintf(stderr, "Could not open file %s: %s\n", argv[2], strerror(errno));
        return 1;
    }

    size_t width = WORLD_RADIUS * 2;
    size_t height = WORLD_RADIUS * 2;

    float radius = width / 100;
    float padding = 2 * radius;

    fprintf(f, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n");
    fprintf(f, "<svg width=\"500\" height=\"500\" viewBox=\"0 0 %f %f\" xmlns=\"http://www.w3.org/2000/svg\">\n", width + padding * 2, height + padding * 2);
    fprintf(f, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" fill=\"black\" />\n", padding - radius, padding - radius, width + 2*radius, height + 2*radius);

    svg_print_city(f, &data.cities[data.tour[data.num-1]], &data.cities[data.tour[0]], "green");
    for(size_t i = 1; i < data.num; ++i) {
        svg_print_city(f, &data.cities[data.tour[i-1]], &data.cities[data.tour[i]], "red");
    }

    fprintf(f, "</svg>\n");

    return 0;
}
