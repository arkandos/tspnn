#!/usr/bin/env python

import math
import os
import pandas as pd
import numpy as np

testdir="./test_data"
outpath = os.path.join(testdir, "stats.csv")

data = []
for dir in os.listdir(testdir):
    dir = os.path.join(testdir, dir)
    if not os.path.isdir(dir):
        continue

    id = os.path.basename(dir)
    best = float(os.popen("./dist/get-cost " + os.path.join(dir, "best.csv")).read())
    greedy = float(os.popen("./dist/get-cost " + os.path.join(dir, "greedy.csv")).read())
    nn = float(os.popen("./dist/get-cost " + os.path.join(dir, "nn.csv")).read())

    data.append({
        'dir': id,
        'best': best,
        'greedy': greedy,
        'nn': nn,
        'best_sqrt': math.sqrt(best),
        'greedy_sqrt': math.sqrt(greedy),
        'nn_sqrt': math.sqrt(nn),
    })

dt = pd.DataFrame(data=data)
dt.sort_values(by='dir', inplace=True)
dt.to_csv(outpath, index=False)
