#!/usr/bin/env python

import sys

import tensorflow as tf
import keras
from keras import layers
import pandas as pd
import numpy as np


MODEL_PATH="./train_data/model"
MAX_CITIES_COUNT=20

def read_input_data(filename):
    dt = pd.read_csv(filename)
    num = len(dt)

    inputs = np.empty((0,MAX_CITIES_COUNT*2))
    labels = np.empty((0,2))

    for i in range(num - 2):
        # rotate such that city i is at position 0
        reordered = dt[i:].reset_index(drop=True) # pd.concat([dt[i:], dt[0:i]], ignore_index=True)
        # Data in row 0 is the origin
        origin_x = reordered['x'][0]
        origin_y = reordered['y'][0]

        # Data in row 1 is the destination
        label_x = reordered['x'][1]
        label_y = reordered['y'][1]

        # Shuffle rows except the first one.
        # shuffled = reordered[1:].sample(frac=1, ignore_index=True)

        # Sort other cities based on their distance from the origin
        sorted = reordered[1:].copy()
        sorted['dist'] = sorted[['x', 'y']].apply(lambda row: np.linalg.norm((row.x-origin_x, row.y-origin_y)), axis=1)
        sorted.sort_values(by='dist', inplace=True, ignore_index=True)

        # collect all values into a simple array that we can then convert to numpy?
        input = [origin_x, origin_y]
        for j in range(len(sorted)):
            input.append(sorted['x'][j])
            input.append(sorted['y'][j])

        # Pad with zeros
        for _ in range(len(input), MAX_CITIES_COUNT * 2):
            input.append(0)

        inputs = np.append(inputs, [input], axis=0)
        labels = np.append(labels, [[label_x, label_y]], axis=0)

    return (inputs, labels)


def main():
    if len(sys.argv) < 2:
        print("Usage:", sys.argv[0], "tour.csv...")
        exit(1)

    print("Loading Model...")

    model = keras.models.load_model(MODEL_PATH)
    print("Training on", len(sys.argv[1:]), "files")

    inputs = np.empty((0,MAX_CITIES_COUNT*2))
    labels = np.empty((0,2))

    for filename in sys.argv[1:]:
        file_inputs, file_labels = read_input_data(filename)
        inputs = np.concatenate((inputs, file_inputs), axis=0)
        labels = np.concatenate((labels, file_labels), axis=0)

    model.compile(optimizer='adam', loss='mean_squared_error')
    model.fit(inputs, labels, epochs=10)

    model.save(MODEL_PATH)


if __name__ == "__main__":
    main()
