#include <stdio.h>

#include "csv.h"
#include "cost_matrix.h"
#include "tour.h"

internal void swap(void *a, void *b, size_t size) {
    char *ac = a;
    char *bc = b;

    for (size_t i = 0; i < size; ++i) {
        char tmp = ac[i];
        ac[i] = bc[i];
        bc[i] = tmp;
    }
}

typedef struct {
    Tour tour;
    size_t num;

    // encoding of the stack state. state[k] encodes the for-loop counter for when generate(k - 1, A) is called
    size_t *state;
    // i acts similarly to a stack pointer
    size_t i;
} Permutation;

internal Permutation permutation_new(Tour tour, size_t num) {
    return (Permutation){
        .tour = tour + 1,
        .num = num - 1,
        //
        .state = calloc(num - 1, sizeof(size_t)),
        .i = 0
    };
}

internal bool permutation_next(Permutation *p) {
    // implementation of Heap's algorithm.
    // See: https://en.wikipedia.org/wiki/Heap%27s_algorithm
    size_t i = p->i;
    // initial output - the initial array configuration is a valid permutation.
    if (i == 0) {
        p->i = 1;
        return true;
    }

    // while (i < n)
    if (i >= p->num) {
        return false;
    }

    if (p->state[i] < i) {
        size_t j = 0;
        if (i % 2) { // i is odd
            j = p->state[i];
        }

        tour_swap_cities(p->tour, j, i);

        // Swap has occurred ending the for-loop. Simulate the increment of the for-loop counter
        p->state[i] += 1;
        // Simulate recursive call reaching the base case by bringing the pointer to the base case analog in the array
        p->i = 1;

        // output(A)
        return true;
    } else {
        // Calling generate(i+1, A) has ended as the for-loop terminated. Reset the state and simulate popping the stack by incrementing the pointer.
        p->state[i] = 0;
        p->i += 1;
        // loop until we are either done or output something.
        return permutation_next(p);
    }
}

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s tour.csv\n", argv[0]);
        return 1;
    }

    CsvData data = csv_load(argv[1]);
    CostMatrix matrix = cost_matrix_new(data.cities, data.num);

    printf("Tour cost: %lu\n", tour_cost(matrix, data.tour, data.num));

    Tour tour = tour_new(data.num);
    memcpy(tour, data.tour, sizeof(tour[0]) * data.num);

    // we do not need to permute the first element, since we go in a circle anyways.
    Permutation p = permutation_new(tour, data.num);
    Cost best_cost = tour_cost(matrix, tour, data.num);
    while(permutation_next(&p)) {
        Cost cost = tour_cost(matrix, tour, data.num);
        if (cost < best_cost) {
            printf("!!! BETTER Tour found with a cost of %lu\n", cost);
            print_tour(data.cities, tour, data.num);
            break;
        }
    }

    printf("This tour is also the best possible permutation.\n");

    return 0;
}
