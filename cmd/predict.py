#!/usr/bin/env python

import sys

import tensorflow as tf
import keras
from keras import layers
import pandas as pd
import numpy as np


MODEL_PATH="./train_data/model"
MAX_CITIES_COUNT=20

def city_distance(city, x, y):
    dx = city.x-x
    dy = city.y-y
    return (dx*dx + dy*dy)


def city_distance2(c1, c2):
    return city_distance(c1, c2.x, c2.y)

def read_tour(filename):
    dt = pd.read_csv(filename)
    return list(dt.itertuples(index=False, name='City'))


def tour_to_inputs(tour):
    inputs = []

    # Since we train on sorted data, we need to predict based on sorted data as well.
    tour = sorted(tour, key=lambda city: city_distance2(tour[0], city))

    for city in tour:
        inputs.append(city.x)
        inputs.append(city.y)

    for _ in range(len(inputs), 2*MAX_CITIES_COUNT):
        inputs.append(0)

    # return np.array(inputs, dtype=float)
    return inputs



def find_closest_city(tour, x, y):
    closest = 0
    closest_dist = city_distance(tour[closest], x, y)

    for idx, city in enumerate(tour[1:]):
        dist = city_distance(city, x, y)
        if dist < closest_dist:
            closest_dist = dist
            closest = idx + 1 # because we slice the array

    return closest


def tour_cost(tour):
    cost = 0

    for i in range(1, len(tour)):
        cost += city_distance2(tour[i-1], tour[i])

    cost += city_distance2(tour[len(tour)-1], tour[0])

    return cost


def tour_save(filename, tour):
    dt = pd.DataFrame(tour)
    if filename == "-":
        print(dt.to_csv(None, index=False))
    else:
        dt.to_csv(filename, index=False)


def main():
    if len(sys.argv) < 2:
        print("Usage:", sys.argv[0], "tour.csv")
        exit(1)

    model = keras.models.load_model(MODEL_PATH)

    tour = read_tour(sys.argv[1])
    print("Cost of tour: ", tour_cost(tour))

    best_tour = []
    best_cost = float('+inf')

    for i in range(0, len(tour)):
        tour_copy = tour.copy()
        # Determine first city.
        tour_copy[0], tour_copy[i] = tour_copy[i], tour_copy[0]

        for j in range(1, len(tour) - 1):
            # predict i-th city.
            [[pred_x, pred_y]] = model.predict([tour_to_inputs(tour_copy[j-1:])])
            # print(pred_x, pred_y)

            # Again, +j because we slice the array
            closest = j + find_closest_city(tour_copy[j:], pred_x, pred_y)
            # print("Closest city: ", pred_x, pred_y, tour[closest])

            # Swap the closest city to the ith place
            tour_copy[j], tour_copy[closest] = tour_copy[closest], tour_copy[j]

        cost = tour_cost(tour_copy)
        print("Found potential tour with cost", cost)
        if cost < best_cost:
            best_cost = cost
            best_tour = tour_copy

    print("Cost of predicted tour: ", best_cost)

    if len(sys.argv) >= 3:
        tour_save(sys.argv[2], best_tour)


if __name__ == '__main__':
    main()

