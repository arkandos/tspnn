#include "csv.h"
#include "tour.h"

internal size_t city_closest(CostMatrix matrix, size_t num, size_t at) {
    size_t closest = -1;
    Cost closest_cost = COST_MAX;

    for(size_t i = 0; i < num; ++i) {
        if (i == at) {
            continue;
        }

        Cost cost = cost_matrix_distance(matrix, num, at, i);
        if (cost < closest_cost) {
            closest_cost = cost;
            closest = i;
        }
    }

    return closest;
}

internal Tour tour_greedy_help(CostMatrix matrix, size_t num, size_t start_at) {
    Tour tour = tour_new(num);
    tour_swap_cities(tour, 0, start_at);

    for(size_t i = 1; i < num; ++i) {
        Cost best_cost = COST_MAX;
        size_t best_idx = -1;

        for(size_t j = i; j < num; ++j) {
            Cost cost = cost_matrix_distance(matrix, num, tour[i - 1], tour[j]);
            if (cost < best_cost) {
                best_cost = cost;
                best_idx = j;
            }
        }

        tour_swap_cities(tour, i, best_idx);
    }

    return tour;
}

internal Tour tour_greedy(CostMatrix matrix, size_t num) {
    Tour best_tour = tour_greedy_help(matrix, num, 0);
    Cost best_cost = tour_cost(matrix, best_tour, num);

    for(size_t i = 1; i < num; ++i) {
        Tour tour = tour_greedy_help(matrix, num, i);
        Cost cost = tour_cost(matrix, tour, num);

        printf("Found a tour with a cost of %lu\n", cost);
        if (cost < best_cost) {
            best_cost = cost;
            best_tour = tour;
        }
    }

    return best_tour;
}


int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s tour.csv [greedy.csv]\n", argv[0]);
        return 1;
    }

    CsvData data = csv_load(argv[1]);
    CostMatrix matrix = cost_matrix_new(data.cities, data.num);
    printf("Tour cost: %lu\n", tour_cost(matrix, data.tour, data.num));

    // shuffle(data.cities, data.num, sizeof(City));
    matrix = cost_matrix_new(data.cities, data.num);

    Tour best_tour = tour_greedy(matrix, data.num);
    printf("Best greedy tour cost: %lu\n", tour_cost(matrix, best_tour, data.num));

    if (argc >= 3) {
        csv_print(argv[2], data.cities, best_tour, data.num);
    }

    return 0;
}
