#!/bin/bash

# PARAMS: MIN_CITY_COUNT=12, MAX_CITY_COUNT=20

for i in {00..99};
do
    dir="./test_data/$i"

    # make sure directory exists.
    mkdir -p "$dir"

    # generate a solution
    ./dist/generate-train-data "$dir/best.csv"; ./dist/to-svg "$dir/best.csv" "$dir/best.svg"

    # generate a greedy version
    ./dist/greedy "$dir/best.csv" "$dir/greedy.csv"; ./dist/to-svg "$dir/greedy.csv" "$dir/greedy.svg"

    #try the NN
    ./dist/predict "$dir/best.csv" "$dir/nn.csv"; ./dist/to-svg "$dir/nn.csv" "$dir/nn.svg"
done

