# Beschreibung und Vergleich eines Feed-Forward neuronalen Netzwerks mit der Nearest-Neighbour-Heuristik, sowie der bestmöglichen Lösung von Traveling-Salesman Problemen

Joshua Reusch, Michael Wallner

## Projektstruktur

| Ordner                    | Beschreibung                                             |
|---------------------------|----------------------------------------------------------|
| `shell.nix`               | Abhängigkeiten und Entwicklungstools (GCC, Python, Make) |
| `Makefile`                | Build-Script                                             |
| `cmd/`                    | Quellordner für Programme                                |
| `dist/`                   | Ausgabeordner für kompilierte Programme                  |
| `lib/`                    | C-Hilfsdateien / Bibliotheken                            |
| `probe/`                  | Erste Testversion, nicht offiziell Teil der Abgabe       |
| `test_data/XX/best.csv`   | Beste Lösung für das Problem                             |
| `test_data/XX/greedy.csv` | Gefundene Lösung des kNN                                 |
| `test_data/XX/nn.csv`     | Gefundene Lösung des NN                                  |
| `train_data/csv/*.csv`    | Trainingsdateien (Vorsicht, großer Ordner)               |


##  Programme

Makefile und alle Programme wurden nur auf Linux getestet.

| Name                    | Beschreibung                                                                   |
|-------------------------|--------------------------------------------------------------------------------|
| `get-cost`              | Berechnet Kosten für die Tour in einer CSV-Datei                               |
| `brute-force`           | Löse ein TSP indem alle Permutationen durchgegangen werden                     |
| `tsp`                   | Löse TSP mithilfe des Branch-and-Bound Algorithmus                             |
| `greedy`                | Löse TSP mithilfe des Greedy-Algorithmus                                       |
| `predict`               | Löse TSP mithilfe des Neuronalen Netzwerks                                     |
| `init-model`            | Neues Keras/Tensorflow Model erstellen (und das alte löschen)                  |
| `train`                 | Trainiere Neuronales Netzwerk anhand der übergebenen Daten                     |
| `generate-test-data`    | Neuen Testdatensatz und alle Lösungen mit allen Algorithmen generieren         |
| `generate-test-summary` | Aus den Testdatensätzen die `stats.csv` Datei mit Zusammenfassungen generieren |
| `generate-train-data`   | Eine zufällige Tour generieren und mit Branch-and-Bound Lösen                  |
| `generate-train-set`    | Neuen Trainingsdatensatz erstellen                                             |
| `to-svg`                | Visualisierung für CSV-Datei                                                   |
