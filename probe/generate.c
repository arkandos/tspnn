#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "city_names.h"

const size_t MIN_CITIES_COUNT = 12;
const size_t MAX_CITIES_COUNT = 20;
const int WORLD_RADIUS = 999;

#define internal static inline
#define UNUSED(x) (void)(x)
#define array_length(arr) (sizeof(arr) / sizeof((arr)[0]))

// [0; 1[
internal float rand_float() {
    return rand() / (RAND_MAX + 1.0);
}

// [min; max]
internal int rand_int(int includingLower, int includingUpper) {
    assert(includingLower <= includingUpper);

    // since casting to int always rounds down, we will
    // never hit excludingUpper, since rand_float() has
    // an excluding upper bound as well.
    int excludingUpper = includingUpper + 1;
    int d = excludingUpper - includingLower;
    return includingLower + (int)(rand_float() * d);
}

#define rand_elem(arr) ((arr)[rand_int(0, array_length(arr) - 1)])


internal void swap(void *a, void *b, size_t size) {
    char *ac = a;
    char *bc = b;

    for (size_t i = 0; i < size; ++i) {
        char tmp = ac[i];
        ac[i] = bc[i];
        bc[i] = tmp;
    }
}


internal void shuffle(void *arr, size_t num, size_t size) {
    char *data = arr;

    for (size_t i = 0; i < size; ++i) {
        size_t j = rand_int(i + 1, num - 1);
        swap(data + i * size, data + j * size, size);
    }
}


int seed = 0;
internal int noise(size_t x, size_t y) {
    static const int hash[] =
        {208,34,231,213,32,248,233,56,161,78,24,140,71,48,140,254,245,255,247,247,40,
        185,248,251,245,28,124,204,204,76,36,1,107,28,234,163,202,224,245,128,167,204,
        9,92,217,54,239,174,173,102,193,189,190,121,100,108,167,44,43,77,180,204,8,81,
        70,223,11,38,24,254,210,210,177,32,81,195,243,125,8,169,112,32,97,53,195,13,
        203,9,47,104,125,117,114,124,165,203,181,235,193,206,70,180,174,0,167,181,41,
        164,30,116,127,198,245,146,87,224,149,206,57,4,192,210,65,210,129,240,178,105,
        228,108,245,148,140,40,35,195,38,58,65,207,215,253,65,85,208,76,62,3,237,55,89,
        232,50,217,64,244,157,199,121,252,90,17,212,203,149,152,140,187,234,177,73,174,
        193,100,192,143,97,53,145,135,19,103,13,90,135,151,199,91,239,247,33,39,145,
        101,120,99,3,186,86,99,41,237,203,111,79,220,135,158,42,30,154,120,67,87,167,
        135,176,183,191,253,115,184,21,233,58,129,233,142,39,128,211,118,137,139,255,
        114,20,218,113,154,27,127,246,250,1,8,198,250,209,92,222,173,21,88,102,219};

    int tmp = hash[(y + seed) % array_length(hash)];
    return hash[(tmp + x) % array_length(hash)];
}


internal double interpolate(int from, int to, double delta) {
    int diff = to - from;
    return from + diff * delta;
}


internal double smooth_interpolate(size_t from, size_t to, double delta) {
    return interpolate(from, to, delta * delta * (3 - 2 * delta));
}


double perlin_noise_help(double x, double y) {
    int xi = x;
    int yi = y;
    double dx = x - xi;
    double dy = y - yi;
    int s = noise(xi, yi);
    int t = noise(xi + 1, yi);
    int u = noise(xi, yi + 1);
    int v = noise(xi + 1, yi + 1);
    double low = smooth_interpolate(s, t, dx);
    double high = smooth_interpolate(u, v, dx);
    return smooth_interpolate(low, high, dy);
}


double perlin_noise(size_t x, size_t y, double freq, size_t depth) {
    double xa = x*freq;
    double ya = y*freq;

    double amp = 1.0;
    double fin = 0;
    double div = 0.0;

    for(size_t i = 0; i < depth; ++i) {
        div += 255 * amp;
        fin += perlin_noise_help(xa, ya) * amp;
        amp /= 2;
        xa *= 2;
        ya *= 2;
    }

    return fin / div;
}




// typedef struct {
//     char *array;
//     size_t num;
//     size_t size;

//     // encoding of the stack state. state[k] encodes the for-loop counter for when generate(k - 1, A) is called
//     size_t *state;
//     // i acts similarly to a stack pointer
//     size_t i;
// } Permutation;

// internal Permutation permutation_new(void *arr, size_t num, size_t size) {
//     return (Permutation){
//         .array = arr,
//         .num = num,
//         .size = size,
//         //
//         .state = calloc(num, sizeof(size_t)),
//         .i = 0
//     };
// }

// internal void permutation_free(Permutation *p) {
//     free(p->state);
// }

// internal bool permutation_next(Permutation *p) {
//     // implementation of Heap's algorithm.
//     // See: https://en.wikipedia.org/wiki/Heap%27s_algorithm
//     size_t i = p->i;
//     // initial output - the initial array configuration is a valid permutation.
//     if (i == 0) {
//         p->i = 1;
//         return true;
//     }

//     // while (i < n)
//     if (i >= p->num) {
//         return false;
//     }

//     if (p->state[i] < i) {
//         size_t j = 0;
//         if (i % 2) { // i is odd
//             j = p->state[i];
//         }

//         swap(p->array + j * p->size, p->array + i * p->size, p->size);

//         // Swap has occurred ending the for-loop. Simulate the increment of the for-loop counter
//         p->state[i] += 1;
//         // Simulate recursive call reaching the base case by bringing the pointer to the base case analog in the array
//         p->i = 1;

//         // output(A)
//         return true;
//     } else {
//         // Calling generate(i+1, A) has ended as the for-loop terminated. Reset the state and simulate popping the stack by incrementing the pointer.
//         p->state[i] = 0;
//         p->i += 1;
//         // loop until we are either done or output something.
//         return permutation_next(p);
//     }
// }


typedef struct {
    char *name;
    int x;
    int y;
} City;

typedef unsigned long Cost;
const Cost COST_MAX = ULONG_MAX / 2;

typedef size_t* Tour;


internal City city_new(size_t i) {
    City result;

    result.name = city_names[i];

    // make sure we do not generate cities on the origin - this is a special
    // point we use to indicate <no data> to the NN.
    do {
        result.x = rand_int(-WORLD_RADIUS, WORLD_RADIUS);
        result.y = rand_int(-WORLD_RADIUS, WORLD_RADIUS);
    } while (!result.x && !result.y);

    return result;
}

internal Cost city_distance(City *c1, City *c2) {
    Cost dx = c1->x - c2->x;
    Cost dy = c1->y - c2->y;

    return (dx*dx + dy*dy);
}

internal Cost* cities_cost_matrix(City *cities, size_t num) {
    Cost *cost_matrix = calloc(num * num, sizeof(Cost));
    for(size_t i = 0; i < num; ++i) {
        for(size_t j = 0; j < num; ++j) {
            // the matrix is symmetric, so we don't need to care about the order etc.
            cost_matrix[i * num + j] = city_distance(&cities[i], &cities[j]);
        }
    }
    return cost_matrix;
}


internal void tour_write_svg(const char *filename, City *cities, Tour tour, size_t num) {
    FILE *f = NULL;

    f = fopen(filename, "w+");
    assert(f);

    size_t width = WORLD_RADIUS * 2;
    size_t height = WORLD_RADIUS * 2;

    size_t radius = width / 100;
    size_t line_width = radius / 3;
    if (line_width == 0) {
        line_width = 1;
    }

    size_t padding = 2 * radius;

    fprintf(f, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n");
    fprintf(f, "<svg width=\"500\" height=\"500\" viewBox=\"0 0 %lu %lu\" xmlns=\"http://www.w3.org/2000/svg\">\n", width + padding * 2, height + padding * 2);
    fprintf(f, "<rect x=\"%lu\" y=\"%lu\" width=\"%lu\" height=\"%lu\" fill=\"black\" />\n", padding - radius, padding - radius, width + 2*radius, height + 2*radius);

    {
        City *city = &cities[tour[0]];
        size_t x = city->x + WORLD_RADIUS + padding;
        size_t y = city->y + WORLD_RADIUS + padding;

        fprintf(f, "<circle cx=\"%lu\" cy=\"%lu\" r=\"%lu\" fill=\"red\" />\n", x, y, radius);

        City *prev = &cities[tour[num-1]];
        size_t px = prev->x + WORLD_RADIUS + padding;
        size_t py = prev->y + WORLD_RADIUS + padding;

        fprintf(f, "<line x1=\"%lu\" y1=\"%lu\" x2=\"%lu\" y2=\"%lu\" stroke=\"red\" stroke-width=\"%lu\" />\n", px, py, x, y, line_width);
    }

    for(size_t i = 1; i < num; ++i) {
        City *city = &cities[tour[i]];
        size_t x = city->x + WORLD_RADIUS + padding;
        size_t y = city->y + WORLD_RADIUS + padding;
        fprintf(f, "<circle cx=\"%lu\" cy=\"%lu\" r=\"%lu\" fill=\"red\" />\n", x, y, radius);

        City *prev = &cities[tour[i-1]];
        size_t px = prev->x + WORLD_RADIUS + padding;
        size_t py = prev->y + WORLD_RADIUS + padding;

        fprintf(f, "<line x1=\"%lu\" y1=\"%lu\" x2=\"%lu\" y2=\"%lu\" stroke=\"red\" stroke-width=\"%lu\" />\n", px, py, x, y, line_width);
    }

    fprintf(f, "<script>setTimeout(() => location.reload(), 1100)</script>\n");

    fprintf(f, "</svg>\n");

    fclose(f);
}


internal Cost cost_matrix_distance(Cost *cost_matrix, size_t num, size_t j, size_t k) {
    return cost_matrix[j * num + k];
}

internal Tour tour_new(size_t num) {
    Tour tour = calloc(num, sizeof(size_t));
    for(size_t i = 0; i < num; ++i) {
        tour[i] = i;
    }

    return tour;
}

internal Cost tour_cost(Cost *cost_matrix, Tour tour, size_t num) {
    Cost result = 0;
    for(size_t idx = 1; idx < num; ++idx) {
        result += cost_matrix_distance(cost_matrix, num, tour[idx-1], tour[idx]);
    }

    // return to start
    result += cost_matrix_distance(cost_matrix, num, tour[0], tour[num - 1]);

    return result;
}

internal void tour_swap_cities(Tour tour, size_t i, size_t j) {
    size_t k = tour[i];
    tour[i] = tour[j];
    tour[j] = k;
}

internal size_t tour_find_city(Tour tour, size_t num, size_t k) {
    for(size_t i = 0; i < num; ++i) {
        if (tour[i] == k) {
            return i;
        }
    }
    return -1;
}

internal void print_tour(City *cities, Tour tour, size_t num) {
    for(size_t i = 0; i < num; ++i) {
        City *c = &cities[tour[i]];
        printf("%2lu. (%3d, %3d) %s\n", i + 1, c->x, c->y, c->name);
    }
}

typedef struct {
    Cost *cost_matrix;
    Cost (*row_mins)[2];
    size_t num;

    Tour tour;

    Cost best_cost;
    Tour best_tour;
} TravelingSalesmanState;


internal void traveling_salesman_help(TravelingSalesmanState *s, size_t level, Cost bound_from_i, Cost curr_cost) {
    size_t i = s->tour[level-1];

    // [0..level-1] is the tour so far
    // [level..num-1] is the cities we havent visited yet.

    for (size_t tk = level, num = s->num; tk < num; ++tk) {
        size_t k = s->tour[tk];

        Cost cost_to_k = curr_cost + cost_matrix_distance(s->cost_matrix, num, i, k);
        // already more expensive, don't bother anymore.
        if (cost_to_k >= s->best_cost) {
            continue;
        }

        // we can visit a city by swapping the level entry with the tk entry,
        // moving the tk entry into the visited portion of the tour and the
        // old level entry into the unvisited part of the tour.
        tour_swap_cities(s->tour, level, tk);

        // we set the second-to-last city. we do not have a choice for the
        // last city anymore since we need to complete the tour
        if (level == num - 2) {
            size_t l = s->tour[num - 1];
            // final costs are the costs so far, the cost from the second-to-last to
            // the last city we just found, and the cost from the last city back to the start.
            Cost final_cost = cost_to_k
                + cost_matrix_distance(s->cost_matrix, num, k, l)
                + cost_matrix_distance(s->cost_matrix, num, l, s->tour[0]);

            if (final_cost < s->best_cost) {
                memcpy(s->best_tour, s->tour, sizeof(s->best_tour[0]) * num);
                s->best_cost = final_cost;
            }
        } else {
            // we visit k, which always means this is the first time visiting this city.
            // we remove the lowest possible cost from our estimate.
            Cost bound_to_k = bound_from_i - s->row_mins[k][0];

            // we only have to explore nodes where the lowest possible estimated cost
            // is smaller than the best solution we found so far - otherwise it can
            // only get worse.

            // * 2 here since our bound is always twice the tour (see traveling_salesman())
            if (bound_to_k + cost_to_k * 2 < s->best_cost * 2) {
                // we already went from i to k, removing the lowest cost.
                // this time, we leave from k before recursing, walking the second path,
                // so we remove the second lowest cost from our estimate.
                Cost bound_from_k = bound_to_k - s->row_mins[k][1];
                traveling_salesman_help(s, level + 1, bound_from_k, cost_to_k);
            }
        }

        tour_swap_cities(s->tour, level, tk);
    }
}

// http://dspace.mit.edu/bitstream/handle/1721.1/46828/algorithmfortrav00litt.pdf
internal void traveling_salesman(Cost *cost_matrix, Tour best_tour, size_t num) {
    // precompute the 2 lowest entries in every row values
    Cost (*row_mins)[2] = calloc(num, sizeof(Cost) * 2);
    for(size_t i = 0; i < num; ++i) {
        row_mins[i][0] = COST_MAX;
        row_mins[i][1] = COST_MAX;

        for(size_t k = 0; k < num; ++k) {
            if (i == k) {
                continue;
            }

            Cost cost = cost_matrix_distance(cost_matrix, num, i, k);
            if (cost <= row_mins[i][0]) {
                row_mins[i][1] = row_mins[i][0];
                row_mins[i][0] = cost;
            } else if (cost < row_mins[i][1]) {
                row_mins[i][1] = cost;
            }
        }
    }

    TravelingSalesmanState state = (TravelingSalesmanState){
        .cost_matrix = cost_matrix,
        .row_mins = row_mins,
        .num = num,

        .tour = tour_new(num),
        .best_tour = best_tour,
        .best_cost = tour_cost(cost_matrix, best_tour, num),
    };

    // initial bound
    Cost lower_bound = 0;
    for(size_t i = 0; i < num; ++i) {
        // we either go through the closest city, or we don't, but we cannot go to the same city twice.
        // at every city, we have an incoming and outgoing path as part of the tour,
        // so the lower bound on a tour through a city is the sum of the 2 smallest edges.
        lower_bound += row_mins[i][0] + row_mins[i][1];
    }

    // curr_bound is now 2 times the "actual" lower bound
    // since we considered 2 edges from/to every city, we have counted (1,3) and
    // (3,1) separately (twice), which means we counted 2 tours.
    // we will not fix this here though, to not break integer maths.

    // visit the first city. Since we always go in a loop, the starting city can be arbitrary.
    state.tour[0] = 0;

    // we just started our tour, so this is the first time going somewhere from i.
    // we remove the lowest possible cost from our estimate.
    Cost bound_from_0 = lower_bound - row_mins[0][0];
    traveling_salesman_help(&state, 1, bound_from_0, 0);

    free(state.tour);
    free(row_mins);
}


void print_csv_header() {
    for(size_t i = 0; i < MAX_CITIES_COUNT; ++i) {
        printf("label%lu,", i);
    }
    for(size_t i = 0; i < MAX_CITIES_COUNT; ++i) {
        printf("x%lu,y%lu,", i, i);
    }
    printf("next\n");
}

void print_csv_tour(City *cities, Tour tour, size_t num, size_t next_city) {
    for(size_t i = 0; i < MAX_CITIES_COUNT; ++i) {
        if (i < num) {
            printf("%s,", cities[tour[i]].name);
        } else {
            printf(",");
        }
    }
    for(size_t i = 0; i < MAX_CITIES_COUNT; ++i) {
        if (i < num) {
            printf("%d,%d,", cities[tour[i]].x, cities[tour[i]].y);
        } else {
            printf(",,");
        }
    }

    printf("%lu\n", tour_find_city(tour, num, next_city));
}



int main(int argc, char **argv) {
    UNUSED(argc);
    UNUSED(argv);

    srand(time(NULL));
    seed = rand();

    shuffle(city_names, array_length(city_names), sizeof(city_names[0]));

    size_t cities_count = rand_int(MIN_CITIES_COUNT, MAX_CITIES_COUNT);
    City *cities = calloc(cities_count, sizeof(City));
    for(size_t i = 0; i < cities_count; ++i) {
        cities[i] = city_new(i);
    }

    Cost *cost_matrix = cities_cost_matrix(cities, cities_count);
    Tour tour = tour_new(cities_count);
    traveling_salesman(cost_matrix, tour, cities_count);

    tour_write_svg("cities.svg", cities, tour, cities_count);



    size_t width = 800;
    size_t height = 600;

    FILE * f = fopen("perlin.ppm", "w+");

    fprintf(f, "P3\n%lu %lu\n255\n", width, height);

    // unsigned char *buffer = malloc(width * height * 3);
    for(size_t y = 0; y < height; ++y) {
        for(size_t x = 0; x < width; ++x) {
            int gray = (0xFF * perlin_noise(x, y, 0.01, 16));
            if (gray <= 0) gray = 0;
            if (gray > 0xFF) gray = 0xFF;

            // size_t i = (x + y * width) * 3;
            // buffer[i+0] = gray;
            // buffer[i+1] = gray;
            // buffer[i+2] = gray;

            fprintf(f, "%d %d %d ", gray, gray, gray);
        }
    }

    // fwrite(buffer, 3, width * height, f);
    fprintf(f, "\n");
    fclose(f);







    // printf("Genrerated %lu unique cities\n", cities_count);

    // printf("Best route costs: %lu\n", tour_cost(cost_matrix, tour, cities_count));
    // print_tour(cities, tour, cities_count);

    // print_csv_header();

    // Tour output_tour = tour_new(cities_count);
    // for(size_t i = 0; i < cities_count; ++i) {
    //     // print the tour (the i-th element of the tour should be the first)
    //     // shuffle all other cities to improve randomness

    //     size_t first = tour[i];
    //     shuffle(output_tour, cities_count, sizeof(output_tour[0]));
    //     // swap the "first" element to the front
    //     tour_swap_cities(output_tour, 0, tour_find_city(output_tour, cities_count, first));

    //     if (i + 1 < cities_count) {
    //         print_csv_tour(cities, output_tour, cities_count, tour[i + 1]);
    //     } else {
    //         print_csv_tour(cities, output_tour, cities_count, tour[0]);
    //     }
    // }

    // Tour best_tour = tour_new(cities, cities_count);
    // int best_tour_cost = tour_cost(cost_matrix, best_tour, cities_count);

    // Permutation p = permutation_new(tour, cities_count, sizeof(tour[0]));
    // while (permutation_next(&p)) {
    //     int permutation_cost = tour_cost(cost_matrix, tour, cities_count);
    //     if (permutation_cost < best_tour_cost) {
    //         best_tour_cost = permutation_cost;
    //         memcpy(best_tour, tour, cities_count * sizeof(tour[0]));
    //     }
    // }

    // printf("\nBest route costs (Brute Force): %d\n", tour_cost(cost_matrix, best_tour, cities_count));
    // print_tour(best_tour, cities_count);

    return 0;
}
