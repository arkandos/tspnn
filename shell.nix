{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
    # nativeBuildInputs is usually what you want -- tools you need to run
    nativeBuildInputs = with pkgs; [
      gcc gnumake
      python310 python310Packages.tensorflow python310Packages.keras python310Packages.pandas
    ];
}
