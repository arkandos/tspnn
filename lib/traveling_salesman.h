#ifndef _LIB_TRAVELING_SALESMAN
#define _LIB_TRAVELING_SALESMAN

#include "city.h"
#include "tour.h"
#include "cost_matrix.h"


// Based on
// http://dspace.mit.edu/bitstream/handle/1721.1/46828/algorithmfortrav00litt.pdf

typedef Cost (*RowMins)[2];

typedef struct {
    Cost *cost_matrix;
    RowMins row_mins;
    size_t num;

    Tour tour;

    Cost best_cost;
    Tour best_tour;
} TravelingSalesmanState;


internal RowMins row_mins_new(CostMatrix cost_matrix, size_t num) {
    RowMins row_mins = calloc(num, sizeof(Cost) * 2);

    for(size_t i = 0; i < num; ++i) {
        row_mins[i][0] = COST_MAX;
        row_mins[i][1] = COST_MAX;

        for(size_t j = 0; j < num; ++j) {
            if (i == j) continue;

            Cost cost = cost_matrix_distance(cost_matrix, num, i, j);
            if (cost <= row_mins[i][0]) {
                row_mins[i][1] = row_mins[i][0];
                row_mins[i][0] = cost;
            } else if (cost < row_mins[i][1]) {
                row_mins[i][1] = cost;
            }
        }
    }

    return row_mins;
}

internal void traveling_salesman_help(TravelingSalesmanState *s, size_t level, Cost bound_from_i, Cost curr_cost) {
    size_t i = s->tour[level-1];

    // [0..level-1] is the tour so far
    // [level..num-1] is the cities we havent visited yet.

    for (size_t tk = level, num = s->num; tk < num; ++tk) {
        size_t k = s->tour[tk];

        Cost cost_to_k = curr_cost + cost_matrix_distance(s->cost_matrix, num, i, k);
        // already more expensive, don't bother anymore.
        if (cost_to_k >= s->best_cost) {
            continue;
        }

        // we can visit a city by swapping the level entry with the tk entry,
        // moving the tk entry into the visited portion of the tour and the
        // old level entry into the unvisited part of the tour.
        tour_swap_cities(s->tour, level, tk);

        // we set the second-to-last city. we do not have a choice for the
        // last city anymore since we need to complete the tour
        if (level == num - 2) {
            size_t l = s->tour[num - 1];
            // final costs are the costs so far, the cost from the second-to-last to
            // the last city we just found, and the cost from the last city back to the start.
            Cost final_cost = cost_to_k
                + cost_matrix_distance(s->cost_matrix, num, k, l)
                + cost_matrix_distance(s->cost_matrix, num, l, s->tour[0]);

            if (final_cost < s->best_cost) {
                memcpy(s->best_tour, s->tour, sizeof(s->best_tour[0]) * num);
                s->best_cost = final_cost;
            }
        } else {
            // we visit k, which always means this is the first time visiting this city.
            // we remove the lowest possible cost from our estimate.
            Cost bound_to_k = bound_from_i - s->row_mins[k][0];

            // we only have to explore nodes where the lowest possible estimated cost
            // is smaller than the best solution we found so far - otherwise it can
            // only get worse.

            // * 2 here since our bound is always twice the tour (see traveling_salesman())
            if (bound_to_k + cost_to_k * 2 < s->best_cost * 2) {
                // we already went from i to k, removing the lowest cost.
                // this time, we leave from k before recursing, walking the second path,
                // so we remove the second lowest cost from our estimate.
                Cost bound_from_k = bound_to_k - s->row_mins[k][1];
                traveling_salesman_help(s, level + 1, bound_from_k, cost_to_k);
            }
        }

        tour_swap_cities(s->tour, level, tk);
    }
}

internal void traveling_salesman(CostMatrix cost_matrix, Tour best_tour, size_t num) {
    TravelingSalesmanState state = (TravelingSalesmanState){
        .num = num,
        .best_tour = best_tour,
        .cost_matrix = cost_matrix,

        .row_mins = row_mins_new(cost_matrix, num),
        .tour = tour_new(num),
        .best_cost = tour_cost(cost_matrix, best_tour, num),
    };

    // initial bound
    Cost lower_bound = 0;
    for(size_t i = 0; i < num; ++i) {
        // we either go through the closest city, or we don't, but we cannot go to the same city twice.
        // at every city, we have an incoming and outgoing path as part of the tour,
        // so the lower bound on a tour through a city is the sum of the 2 smallest edges.
        lower_bound += state.row_mins[i][0] + state.row_mins[i][1];
    }

    // curr_bound is now 2 times the "actual" lower bound
    // since we considered 2 edges from/to every city, we have counted (1,3) and
    // (3,1) separately (twice), which means we counted 2 tours.
    // we will not fix this here though, to not break integer maths.

    // visit the first city. Since we always go in a loop, the starting city can be arbitrary.
    state.tour[0] = 0;

    // we just started our tour, so this is the first time going somewhere from i.
    // we remove the lowest possible cost from our estimate.
    Cost bound_from_0 = lower_bound - state.row_mins[0][0];
    traveling_salesman_help(&state, 1, bound_from_0, 0);
}

#endif
