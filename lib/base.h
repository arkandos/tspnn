#ifndef _LIB_BASE
#define _LIB_BASE

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#define internal static inline
#define UNUSED(x) (void)(x)
#define array_length(arr) (sizeof(arr) / sizeof((arr)[0]))

const size_t MIN_CITIES_COUNT = 12;
const size_t MAX_CITIES_COUNT = 20;
const int WORLD_RADIUS = 999;

#endif
