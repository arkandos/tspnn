#ifndef _LIB_CITY
#define _LIB_CITY

#include <limits.h>

#include "base.h"
#include "city_names.h"

typedef struct {
    char *name;
    int x;
    int y;
} City;


typedef unsigned long Cost;
const Cost COST_MAX = ULONG_MAX / 2;

internal Cost city_distance(City *c1, City *c2) {
    Cost dx = c1->x - c2->x;
    Cost dy = c1->y - c2->y;

    return (dx*dx + dy*dy);
}

#endif
