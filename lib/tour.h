#ifndef _LIB_TOUR
#define _LIB_TOUR

#include <stdlib.h>

#include "base.h"
#include "cost_matrix.h"

typedef size_t *Tour;

internal Tour tour_new(size_t num) {
    Tour tour = calloc(num, sizeof(size_t));
    for(size_t i = 0; i < num; ++i) {
        tour[i] = i;
    }

    return tour;
}

internal Cost tour_cost(CostMatrix cost_matrix, Tour tour, size_t num) {
    Cost result = 0;
    for(size_t idx = 1; idx < num; ++idx) {
        result += cost_matrix_distance(cost_matrix, num, tour[idx-1], tour[idx]);
    }

    // return to start
    result += cost_matrix_distance(cost_matrix, num, tour[0], tour[num - 1]);

    return result;
}

internal void tour_swap_cities(Tour tour, size_t i, size_t j) {
    size_t k = tour[i];
    tour[i] = tour[j];
    tour[j] = k;
}

internal size_t tour_find_city(Tour tour, size_t num, size_t k) {
    for(size_t i = 0; i < num; ++i) {
        if (tour[i] == k) {
            return i;
        }
    }
    return -1;
}


internal void print_tour(City *cities, Tour tour, size_t num) {
    for(size_t i = 0; i < num; ++i) {
        City *c = &cities[tour[i]];
        printf("%2lu. (%3d, %3d) %s\n", i + 1, c->x, c->y, c->name);
    }
}

#endif
