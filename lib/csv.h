#ifndef _LIB_LOAD
#define _LIB_LOAD

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "city.h"
#include "tour.h"

typedef struct CsvData {
    City *cities;
    Tour tour;
    size_t num;
} CsvData;


internal CsvData csv_load(const char *filename) {
    // read entire file.
    FILE *f = fopen(filename, "r");
    if (!f) {
        fprintf(stderr, "Could not open file %s: %s\n", filename, strerror(errno));
        exit(1);
    }

    fseek(f, 0, SEEK_END);
    size_t len = ftell(f);
    fseek(f, 0, SEEK_SET);

    char *str = malloc(len + 1);
    if (fread(str, 1, len, f) != len) {
        fprintf(stderr, "Coud not read file %s: %s\n", filename, strerror(errno));
        exit(1);
    }
    str[len] = 0;
    fclose(f);

    // first line is headers, skip it
    str = strchr(str, '\n') + 1;

    // count remaining newlines, this gives us the number of cities.
    size_t lines = 0;
    for(char *s = str; s; s = strchr(s, '\n')) {
        lines += 1;
        s += 1;
    }

    assert(lines > 0);

    size_t num = lines - 1;
    City *cities = calloc(num, sizeof(City));

    // the name of the first city is the correct order.
    for(size_t i = 0; i < num; ++i) {
        char *next = strchr(str, '\n') + 1;

        char *name_start = str;
        char *name_end = strchr(name_start, ',');
        *name_end = 0;

        cities[i].name = name_start;

        char *x_start = name_end + 1;
        char *y_start = strchr(x_start, ',') + 1;

        cities[i].x = atoi(x_start);
        cities[i].y = atoi(y_start);

        str = next;
    }

    return (CsvData){
        .cities = cities,
        .tour = tour_new(num),
        .num = num
    };
}


internal void csv_print(const char *filename, City *cities, Tour tour, size_t num) {
    FILE *f;
    if (strcmp(filename, "-") == 0) {
        f = stdout;
    } else {
        f =fopen(filename, "w+");
    }

    assert(f);

    fprintf(f, "label,x,y\n");

    for(size_t i = 0; i < num; ++i) {
        City *city = &cities[tour[i]];
        fprintf(f, "%s,%d,%d\n", city->name, city->x, city->y);
    }

    fclose(f);
}


#endif
