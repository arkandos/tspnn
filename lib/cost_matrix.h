#ifndef _LIB_COST_MATRIX
#define _LIB_COST_MATRIX

#include "city.h"

typedef Cost *CostMatrix;

internal CostMatrix cost_matrix_new(City *cities, size_t num) {
    CostMatrix cost_matrix = calloc(num * num, sizeof(Cost));

    for(size_t i = 0; i < num; ++i) {
        for(size_t j = 0; j < num; ++j) {
            cost_matrix[i * num + j] = city_distance(&cities[i], &cities[j]);
        }
    }

    return cost_matrix;
}

internal Cost cost_matrix_distance(CostMatrix cost_matrix, size_t num, size_t j, size_t k) {
    return cost_matrix[j * num + k];
}

#endif
