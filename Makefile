CC ?= gcc
CFLAGS += -Wall -Wextra -pedantic --std=c11 -Ofast -march=native
CFLAGS += -Ilib
CFLAGS += -lm

INPUTS := $(wildcard cmd/*)
OUTPUTS := $(foreach input,$(INPUTS),dist/$(notdir $(basename $(input))))

.PHONY: all
all: $(OUTPUTS)

dist/%: cmd/%.c lib/*.h
	$(CC) $(CFLAGS) $< -o $@

dist/%: cmd/%.sh
	cp $< $@
	chmod +x $@

dist/%: cmd/%.py
	cp $< $@
	chmod +x $@

.PHONY: clean
clean:
	rm -rf dist/*

